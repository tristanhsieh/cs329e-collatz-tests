#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    def test_read2 (self) :
        s    = "2 0\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 2 )
        self.assertEqual(j, 0)

    def test_read3 (self) :
        s    = "23050 53559\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  23050)
        self.assertEqual(j,  53559)

    


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 2,15,25)
        self.assertEqual(w.getvalue(), "2 15 25\n")
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 30,50,80)
        self.assertEqual(w.getvalue(), "30 50 80\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("2 7\n225 260\n121 123\n10000 11111\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 7 17\n225 260 128\n121 123 96\n10000 11111 268\n")
    
    def test_solve3(self):

        r = StringIO("36662 26350\n21 431\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "36662 26350 324\n21 431 144\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
Ran 13 tests in 0.215s

OK
Wiliams-MacBook-Pro:collatz williamkwon$ coverage report -m
Name             Stmts   Miss  Cover   Missing
----------------------------------------------
Collatz.py          39      0   100%
TestCollatz.py      60      0   100%
----------------------------------------------
TOTAL               99      0   100%

"""
